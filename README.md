# Aplicación Android pmdm-pr1-calculator - Calculadora #

*Comparto el siguiente código realizado en el Técnico Superior de Desarrollo de Aplicaciones Multiplataforma.

*Se realizó como una práctica de la asignatura de “Programación Multimedia y Dispositivos Móviles”, es una aplicación Android.

*Hay que tener en cuenta que sólo se pudo utilizar las tecnologías y técnicas dadas en el trimestre de la práctica.

*La práctica se realizó el 12-12-2012

===================


## Texto de la práctica ##

"Depuración de una aplicación mediante LogCat"

Práctica del Primer Trimestre

Programación Multimedia y Dispositivos Móviles

## Supuesto ##
 
Durante el primer trimestre hemos  la arquitectura de la plataforma Android, cómo hacer aplicaciones sencillas y a depurar errores sobre la misma. 

Realizaremos una aplicación en la que introduciremos aposta un error, que depuraremos.

Realizaremos los siguientes pasos: 

1. Crearemos una aplicación sencilla que funcionará. 
2. Introduciremos un error aposta. 
3. La ejecutaremos para ver el error. 
4. Desde la herramienta LogCat detectaremos el error.

## Nota ##

El error que pedían en la práctica e implemente fue (en el código expuesto está corregido):

A la hora de realizar una división he añadido un elementos para que la operación de un error de división por cero.

En el método “operacion” he forzado que la variable operador1 se convierta a entero y se divida por 0.

}else if(operador.contentEquals("/")){
	result=(int)this.operador1/0;
 }

Si la división es con numeradores double devuelve “infinity” pero no da un error como cuando es con operadores int, por eso he forzado que el numerador (operador1) se convierta a int.

## Screenshots ##

Aplicación funcionando

[![Aplicación funcionando](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr1-calculator/pantalla1.png)](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr1-calculator/pantalla1.png)

Control de errores con LogCat

[![Control de errores con LogCat](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr1-calculator/pantalla2.png)](http://fjnavarro.com/assets/screenshot-bitbucket/pmdm-pr1-calculator/pantalla2.png)

## Autor ##

* Francisco José Navarro García <fran@fjnavarro.com>
* Twitter : *[@fjnavarro_](https://twitter.com/fjnavarro_)*
* Linkedin: *[https://www.linkedin.com/in/fjnavarrogarcia](https://www.linkedin.com/in/fjnavarrogarcia)*
* Blog    : *[http://www.fjnavarro.com/](http://www.fjnavarro.com/)*