package com.fjnavarro.android.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.util.Log;

public class MainActivity extends Activity {
	protected double operador1, operador2;
	protected boolean paraOperar, siguienteOperador, devueltoResultado;
	private Button BtnBoton1 ,BtnBoton2 ,BtnBoton3 ,BtnBoton4 ,BtnBoton5 
	,BtnBoton6 ,BtnBoton7 ,BtnBoton8 ,BtnBoton9;	
	private TextView Pantalla, PantOperador;
	
	private static final String LOGTAG = "Calculadora Android";
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //inicializamos
        this.operador1 = 0;
        this.paraOperar = false;
        this.siguienteOperador = false;
        this.devueltoResultado = false;
        
        //Obtenemos una referencia a los controles de la interfaz
        this.Pantalla = (TextView)findViewById(R.id.Pantalla);
        this.PantOperador = (TextView)findViewById(R.id.PantOperador);
        this.BtnBoton1 = (Button)findViewById(R.id.BtnBoton1);
        this.BtnBoton2 = (Button)findViewById(R.id.BtnBoton2);
        this.BtnBoton3 = (Button)findViewById(R.id.BtnBoton3);
        this.BtnBoton4 = (Button)findViewById(R.id.BtnBoton4);
        this.BtnBoton5 = (Button)findViewById(R.id.BtnBoton5);
        this.BtnBoton6 = (Button)findViewById(R.id.BtnBoton6);
        this.BtnBoton7 = (Button)findViewById(R.id.BtnBoton7);
        this.BtnBoton8 = (Button)findViewById(R.id.BtnBoton8);
        this.BtnBoton9 = (Button)findViewById(R.id.BtnBoton9);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void botonNum(View v){
    	// Obtenemos el bot�n pulsado
    	Button ButtonEnUso = (Button)v;
    	
    	Log.d(LOGTAG, "----boton num-----");
    	Log.d(LOGTAG, "paraOperar->"+this.paraOperar);
    	Log.d(LOGTAG, "operador2->"+this.operador2);
    	Log.d(LOGTAG, "operador1->"+this.operador1);
    	Log.d(LOGTAG, "devueltoResultado->"+this.devueltoResultado);
    	Log.d(LOGTAG, "siguienteOperador->"+this.siguienteOperador);
    	Log.d(LOGTAG, "-----------------");
    	
    	// se puede operar
    	if(this.siguienteOperador==true){
    		// borramos la pantalla del operador
    		this.Pantalla.setText("");
    		this.siguienteOperador=false;
    		
    		if(this.devueltoResultado){
    			this.paraOperar=false;
    			this.devueltoResultado=false;
        	}else{
        		this.paraOperar=true;
        	}
    	}
    	// a�adimos n�meros a la pantalla
    	this.Pantalla.setText(this.Pantalla.getText().toString()+ButtonEnUso.getText().toString());
    }
    
    public void botonOper(View v){
    	// Obtenemos el boton pulsado
    	Button ButtonEnUso = (Button)v;  	

    	if(this.devueltoResultado){
    		this.devueltoResultado=false;
    	}
    	
    	Log.d(LOGTAG, "----inicio-----");
    	Log.d(LOGTAG, "paraOperar->"+this.paraOperar);
    	Log.d(LOGTAG, "operador2->"+this.operador2);
    	Log.d(LOGTAG, "operador1->"+this.operador1);
    	Log.d(LOGTAG, "devueltoResultado->"+this.devueltoResultado);
    	Log.d(LOGTAG, "siguienteOperador->"+this.siguienteOperador);
    	Log.d(LOGTAG, "-----------------");
    	
    	if(this.paraOperar==true){
    		// metemos el contenido de la pantalla al operador2
    		this.operador2=Double.valueOf(this.Pantalla.getText().toString());
    		
    		// hacemos la operacion
    		this.operador1=this.operacion();
    		
    		this.operador2=0;
    		
    		// pintamos en la pantalla el valor del operador1
    		this.Pantalla.setText(String.valueOf(this.operador1));
    	}else{
    		// metemos el contenido de la pantalla al operador1
    		this.operador1=Double.valueOf(this.Pantalla.getText().toString());
    	}
    	
    	// cambiamos el operador en la pantalla
    	if(ButtonEnUso.getText().toString().contentEquals("=")){
    		this.PantOperador.setText("");
    		this.devueltoResultado=true;
    	}else{
    		this.PantOperador.setText(ButtonEnUso.getText().toString());
    	}
    	
    	this.siguienteOperador=true;
    	
    	Log.d(LOGTAG, "-------------");
    	Log.d(LOGTAG, "paraOperar->"+this.paraOperar);
    	Log.d(LOGTAG, "operador2->"+this.operador2);
    	Log.d(LOGTAG, "operador1->"+this.operador1);
    	Log.d(LOGTAG, "devueltoResultado->"+this.devueltoResultado);
    	Log.d(LOGTAG, "siguienteOperador->"+this.siguienteOperador);
    	Log.d(LOGTAG, "----fin-----");
    	
    }
    
    private double operacion(){
    	double result=0;    	
    	
    	// hay un operador activado
        if(!this.PantOperador.getText().toString().equals("")){
        	// realizar operaciones
        	String operador = this.PantOperador.getText().toString();
        	
        	try
        	{
        		if(operador.contentEquals("+")){
            		result=this.operador1+this.operador2;
            	}else if(operador.contentEquals("-")){
            		result=this.operador1-this.operador2;
            	}else if(operador.contentEquals("*")){
            		result=this.operador1*this.operador2;
            	}else if(operador.contentEquals("/")){
            		result=this.operador1/this.operador2;
            	}
        	}
        	catch(Exception ex)
        	{
        	    Log.e(LOGTAG, "Fallo en la operaci�n", ex);
        	}        	
        	
        	// activamos el operar
        	this.paraOperar=false;
        }

    	return result;
    }
    
    public void botonEqual(View v){
    	// Obtenemos el botón pulsado
    	Button ButtonEnUso = (Button)v;
	
    	// añadimos el operador en la pantalla
    	ButtonEnUso.getText().toString();
    }
}
